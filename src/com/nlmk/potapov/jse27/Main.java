package com.nlmk.potapov.jse27;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Main {

    public static void main(String[] args) {
        CalculatorImpl calculator = new CalculatorImpl();
        InvocationHandler handler = new InvocationHandlerImpl(calculator);
        ICalculator proxy = (ICalculator) Proxy.newProxyInstance(calculator.getClass().getClassLoader(), calculator.getClass().getInterfaces(), handler);
        for (int i = 1; i <= 10; i++) {
            System.out.println(proxy.getFactorial(i));
        }
        System.out.println(proxy.getFactorial(3));
        System.out.println(proxy.getFactorial(5));
        System.out.println(proxy.getFactorial(17));
        System.out.println(proxy.getFactorial(5));
        System.out.println(proxy.getFactorial(1));
        System.out.println(proxy.getFactorial(-1));
        System.out.println(proxy.getFactorial(2));
        System.out.println(proxy.getFactorial(7));
    }
}
