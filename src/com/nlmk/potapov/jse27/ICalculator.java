package com.nlmk.potapov.jse27;

public interface ICalculator {

    Long getFactorial(Integer x);

}
