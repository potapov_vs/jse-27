package com.nlmk.potapov.jse27;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

public class InvocationHandlerImpl implements InvocationHandler {

    private static final int MAX_ENTRIES = 10;
    private final CalculatorImpl calculator;
    private final LinkedHashMap<Integer, Long> cacheMap;

    public InvocationHandlerImpl(CalculatorImpl calculator) {
        this.calculator = calculator;
        cacheMap = new LinkedHashMap<>(MAX_ENTRIES, 0.75F, false) {

            @Override
            protected boolean removeEldestEntry(Map.Entry eldest) {
                return size() > MAX_ENTRIES;
            }

        };
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (cacheMap.containsKey(args[0])) {
            System.out.print("Значение получено из кэша: " + args[0] + "! = ");
            return cacheMap.get(args[0]);
        }
        System.out.print("Новое рассчитанное значение: " + args[0] + "! = ");
        Long result = (Long) method.invoke(calculator, args);
        cacheMap.put((Integer) args[0], result);
        return result;
    }

}
