package com.nlmk.potapov.jse27;

public class CalculatorImpl implements ICalculator {
    @Override
    public Long getFactorial(Integer x) {
        if (x < 0) {
            return null;
        }
        if (x.equals(0)) {
            return 1L;
        }
        Long result = 1L;
        while (x > 1) {
            result *= x;
            x--;
        }
        return result;
    }
}
